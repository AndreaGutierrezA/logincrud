import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  IDataLogin,
  ILoginForm,
  ILoginResponse,
} from '../componentes/interfaces';
import { StorageManagerService } from './storage-manager.service';
import { TokenManagerService } from './token-manager.service';

@Injectable({
  providedIn: 'root',
})
export class LoginService {

  private $isLogged: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  private $dataLogin: BehaviorSubject<IDataLogin | undefined> =
    new BehaviorSubject<IDataLogin | undefined>(undefined);

  constructor(
    protected http: HttpClient,
    private storageManagerService: StorageManagerService,
    private tokenManagerService: TokenManagerService
  ) {
    this.checkLogin(); 
  }

  get isLogged(): Observable<boolean> {
    return this.$isLogged;
  }

  setLogged(isLogged: boolean): void {
    this.$isLogged.next(isLogged);
  }

  get dataLogin(): Observable<IDataLogin | undefined> {
    return this.$dataLogin;
  }

  setDataLogin(dataLogin: IDataLogin | undefined): void {
    this.$dataLogin.next(dataLogin);
  }

  logout() {
    this.storageManagerService.destroy('dataLogin');
    this.tokenManagerService.destroy();
    this.checkLogin();
  }
  checkLogin() {
    const dataLogin: IDataLogin = this.storageManagerService.get('dataLogin') as IDataLogin;
    const token: string|null = this.tokenManagerService.get();
    if (dataLogin && token) {
      this.setLogged(true);
      this.setDataLogin(dataLogin);
    } else {
      this.setLogged(false);
      this.setDataLogin(undefined)
    }
  }

  verifyLogged(): boolean {
    return this.tokenManagerService.get() ? true : false;
  }
}
