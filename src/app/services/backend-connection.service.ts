import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenManagerService } from './token-manager.service';
import { environment } from '../../environments/environment';
import { map, tap } from 'rxjs/operators';
import { StorageManagerService } from './storage-manager.service';
import { LoginService } from './login.service';

export class BackendConnection {
  url = environment.backendUrl;

  constructor(
    protected http: HttpClient,
    protected storageManagerService: StorageManagerService,
    protected tokenManagerService: TokenManagerService,
    protected loginService: LoginService
  ) {}

  getAll<T>(endpoint: string): Observable<T | null> {
    return this.http.get<T>(this.url + endpoint);
  }

  getOne<T>(endpoint: string, id: string): Observable<T | null> {
    return this.http
      .get<T>(this.url + endpoint + '/' + id);
  }

  post<T>(endpoint: string, payload: any): Observable<T | null> {
    return this.http
      .post<T>(this.url + endpoint, payload);
  }

  put<T>(endpoint: string, id: string, payload: object): Observable<T | null> {
    return this.http
      .put<T>(this.url + endpoint + '/' + id, payload);
  }

  delete<T>(endpoint: string, entity: string): Observable<T | null> {
    return this.http
      .delete<T>(this.url + endpoint + '/' + entity);
  }
}
