import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root',
})
/* implements CanActivate, CanActivateChild, CanDeactivate<unknown>, CanLoad { */
export class GuardService implements CanActivate {

  constructor(
    private loginService: LoginService,
    private router: Router
  ) {}

  canActivate(): boolean {
    if (!this.loginService.verifyLogged()) {
      this.loginService.logout();
      return false;
    }
    return true;
  }
}
