import { Injectable } from '@angular/core';
import { BackendConnection } from './backend-connection.service';
import { HttpClient } from '@angular/common/http';
import { TokenManagerService } from './token-manager.service';
import { StorageManagerService } from './storage-manager.service';
import { LoginService } from './login.service';
import { map, Observable, Subject } from 'rxjs';
import { BackendServiceInterface, IParque, IResponse } from '../componentes/interfaces';

@Injectable({
  providedIn: 'root'
})
export class APIService extends BackendConnection implements BackendServiceInterface {
  endpoint = '/jsserver';

  private $reload: Subject<void> = new Subject<void>();

  constructor(
    protected override http: HttpClient,
    protected override storageManagerService: StorageManagerService,
    protected override tokenManagerService: TokenManagerService,
    protected override loginService: LoginService

  ) {
    super(http, storageManagerService, tokenManagerService, loginService);
  }

  get reload(): Observable<void> {
    return this.$reload;
  }

  setReload(): void {
    this.$reload.next();
  }

  //2.1Método de inicio de sesión:
  login(credentials: ILogin): Observable<IDataPropertiesLogin | null> {
    const payload = {
      IDClient: '$#HHJGUY9773H5MNKD65389745KJDFGDGG==',
      ServiceName: "AdminService",
      MethodHash: "com.advantage.bean.account.WorkSession_loguinUsuarioWS_String_String",
      ArgumentList: [
        credentials.login,
        credentials.password
      ]
    };
    const data = JSON.stringify(payload);
    return super.post<IResponseLogin>(this.endpoint, data).pipe(
      map((resp): IDataPropertiesLogin | null => {
        const data = resp ? resp?.DataBeanProperties.ObjectValue.DataBeanProperties : null;
        return data;
      })
    );
  }

  //2.1Consultar todas las propiedades
  getPropiedadesCatalogPorPropiedad(): Observable<IDataProperties[]> {
    const payload = {
      IDClient: '$#HHJGUY9773H5MNKD65389745KJDFGDGG==',
      WSToken: '$#HHJGUYUHSDFGS546546DFH654SGHUJJFF==',
      ServiceName: "AdminService",
      MethodHash: "java.util.List_getPropiedadesCatalogPorPropiedad_String_Object_Number",
      ArgumentList: [
        null,
        null,
        null
      ]
    };
    const data = JSON.stringify(payload);
    return super.post<IResponseProperties>(this.endpoint, data).pipe(
      map((resp): IDataProperties[] => {
        const data = resp ? resp?.DataBeanProperties.ObjectValue.map(
          (response) => {
            return response.DataBeanProperties;
          }
        ) : [];
        return data;
      })
    );
  }

  //2.2 Consultar por IDPropiedades:
  getPropiedadNumber(idPropiedad: number): Observable<IDataProperties> {
    const payload = {
      IDClient: '$#HHJGUY9773H5MNKD65389745KJDFGDGG==',
      ServiceName: "AdminService",
      MethodHash: "com.admin.bean.Propiedades_getPropiedades_Number",
      ArgumentList: [
        idPropiedad
      ]
    };
    const data = JSON.stringify(payload);
    return super.post<IResponseProperty>(this.endpoint, data)
      .pipe(
        map((resp): IDataProperties => {
          const dataProperty: IDataProperties = {
            Nombre: '',
            IDPropiedades: 0,
            Since: '',
            Descripcion: '',
            Valor: '',
            IDPerfilBanco: '',
            Estado: 0
          };
          const data = resp ? resp?.DataBeanProperties.ObjectValue.DataBeanProperties : dataProperty
          return data;
        }
        )
      );
  }

  //2.3Método de Crear y Editar Propiedad:
  updatePropiedades(dataProperty: IDataProperties): Observable<any> {
    const payload = {
      IDClient: '$#HHJGUY9773H5MNKD65389745KJDFGDGG==',
      WSToken: '$#HHJGUYUHSDFGS546546DFH654SGHUJJFF==',
      ServiceName: "AdminService",
      MethodHash: "com.admin.bean.Propiedades_updatePropiedades_com.admin.bean.Propiedades",
      ArgumentList: [
        {
          DataBeanProperties: dataProperty,
          DataBeanName: 'com.admin.bean.Propiedades'
        }
      ]
    };
    const data = JSON.stringify(payload);
    return super.post<IResponseProperty>(this.endpoint, data);
  }

  //2.4 Eliminar Propiedad:
  deletePropiedad(idPropiedad: number | null): Observable<IDataProperties> {
    const payload = {
      IDClient: '$#HHJGUY9773H5MNKD65389745KJDFGDGG==',
      ServiceName: "AdminService",
      MethodHash: "boolean_deletePropiedades_Number",
      ArgumentList: [
        idPropiedad
      ]
    };
    const data = JSON.stringify(payload);
    return super.post<IResponseProperty>(this.endpoint, data)
      .pipe(
        map((resp): IDataProperties => {
          const dataProperty: IDataProperties = {
            Nombre: '',
            IDPropiedades: 0,
            Since: '',
            Descripcion: '',
            Valor: '',
            IDPerfilBanco: '',
            Estado: 0
          };
          const data = resp ? resp?.DataBeanProperties.ObjectValue.DataBeanProperties : dataProperty
          return data;
        }
        )
      );
  }
}



export interface ILogin {
  login: string;
  password: string
}

// login
export interface IResponseAdmin {
  DataBeanName: string;
  DataBeanProperties: {
    Type: number;
    ObjectValue: {
      DataBeanProperties: IDataBeanProperties;
      DataBeanName: string;
    },
    ClassName: string;
    Date: string;
  },
}

// data login
export interface IDataBeanProperties {
  msg: string;
  Account: {
    DataBeanProperties: {
      msg: string;
      User: string;
      EntityName: string;
      PDF417Str: string;
      IDFunctionalLn: number;
      Surname1: string;
      Surname2: string;
      Grade: string;
      IDAccount: number;
      RoleID: number;
      eMail: string;
      TicketCode: number;
      Active: true,
      State: number;
      Nit: number;
      Since: string;
      Tel: string;
      cambiar: false,
      Name1: string;
      Age: number;
      Name2: string;
      Password: string;
    },
    DataBeanName: string;
  },
  IDDocument: string;
  IDSession: number;
  bloqueado: false,
  MacAddress: string;
  IDAccount: number;
  InitDate: string;
  IDOffice: number;
  Estado: string;
  Type: number;
  FinalDate: string;
  LeftDate: string;
  LeftValue: string;
  State: number;
  IPAddress: string;
  HostName: string;
}

// propiedades
export interface IResponseProperties {
  DataBeanName: string;
  DataBeanProperties: IDataBeanPropertiesProperties
}
// para el idProperty
export interface IResponseProperty {
  DataBeanName: string;
  DataBeanProperties: IDataBeanPropertiesProperty
}

export interface IDataBeanPropertiesProperties {
  Type: number;
  ObjectValue: IObjectValue[];
  ClassName: string;
  Date: string;
}

// para el idProperty
export interface IDataBeanPropertiesProperty {
  Type: number;
  ObjectValue: IObjectValue;
  ClassName: string;
  Date: string;
}

export interface IObjectValue {
  DataBeanName: string;
  DataBeanProperties: IDataProperties;
}

export interface IDataProperties {
  Nombre: string;
  IDPropiedades: number | null;
  Since: string;
  Descripcion: string;
  Valor: string;
  IDPerfilBanco: string;
  Estado: number;
}

export interface IResponseLogin {
  DataBeanProperties: {
    Type: number;
    ObjectValue: {
      DataBeanProperties: IDataPropertiesLogin;
      DataBeanName: string;
    },
    ClassName: string;
    Date: string;
  },
  DataBeanName: string;
}
export interface IDataPropertiesLogin {
  msg: string;
  Account: IAcountLogin | null,
  IDDocument: string | null;
  IDSession: number | null;
  bloqueado: boolean,
  MacAddress: string | null;
  IDAccount: number | null;
  InitDate: string | null;
  IDOffice: number | null;
  Estado: string;
  Type: number | null;
  FinalDate: string | null;
  LeftDate: string | null;
  LeftValue: string | null;
  State: number;
  IPAddress: string | null;
  HostName: string | null;
}
export interface IAcountLogin {
  DataBeanProperties: {
    msg: string;
    User: string;
    EntityName: string;
    PDF417Str: string;
    IDFunctionalLn: 1,
    Surname1: string;
    Surname2: string;
    Grade: string;
    IDAccount: number;
    RoleID: number;
    eMail: string;
    TicketCode: number;
    Active: true,
    State: number;
    Nit: number;
    Since: string;
    Tel: string;
    cambiar: false,
    Name1: string;
    Age: number;
    Name2: string;
    Password: string;
  },
  DataBeanName: string;
}