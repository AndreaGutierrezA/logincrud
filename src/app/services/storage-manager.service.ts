import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StorageManagerService {
  constructor() {}

  set(key: string, object: object | string): void {
    localStorage.setItem(key, JSON.stringify(object));
  }

  get(key: string): unknown | null {
    return localStorage.getItem(key)
      ? JSON.parse(localStorage.getItem(key) as string)
      : null;
  }

  destroy(key: string): void {
    localStorage.removeItem(key);
  }

  length(key: string): number | null {
    const typeOfTheVariable = typeof this.get(key);
    let returnValue;
    switch (typeOfTheVariable) {
      case 'string':
        returnValue = (this.get(key) as string).length;
        break;
      case 'object':
        returnValue = Array.isArray(this.get(key))? (this.get(key) as any).length : null;
        break;
      default:
        returnValue = null;
        break;
    }
    return returnValue;
  }
}
