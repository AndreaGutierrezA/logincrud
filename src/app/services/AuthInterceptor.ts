import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenManagerService } from './token-manager.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(protected tokenManagerService: TokenManagerService) {}

  intercept<T>(
    req: HttpRequest<T>,
    next: HttpHandler
  ): Observable<HttpEvent<T>> {
    const authReq = req.clone({
      headers: req.headers.set('Content-Type', 'application/x-www-form-urlencoded'),
    });
    return next.handle(authReq);
  }
}
