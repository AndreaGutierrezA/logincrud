import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { StorageManagerService } from 'src/app/services/storage-manager.service';
import { IDataLogin } from '../interfaces';

@Component({
  selector: 'app-login-box',
  templateUrl: './login-box.component.html',
  styleUrls: ['./login-box.component.scss'],
})
export class LoginBoxComponent implements OnInit {
  isLogged: boolean = false;

  data: IDataLogin | undefined;
  // data: dataLogin = {
  //   usuario: 'Juan Pablo Leon Bazante',
  //   role: 'Administrador',
  // };

  actions = {
    login: {
      label: 'Ingresar',
      path: 'login',
    },
    logout: {
      label: 'Salir',
      path: 'logout',
    },
  };

  constructor(
    private loginService: LoginService,
    private storageManagerService: StorageManagerService
  ) {
    this.loginService.isLogged.subscribe((isLogged) => {
      this.isLogged = isLogged;
    });
    this.loginService.dataLogin.subscribe((dataLogin) => {
      this.data = dataLogin;
    });
  }

  ngOnInit(): void {}
}
