import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { APIService } from 'src/app/services/api.service';
import { LoginService } from 'src/app/services/login.service';
import { StorageManagerService } from 'src/app/services/storage-manager.service';
import { TokenManagerService } from 'src/app/services/token-manager.service';
import { IDataLogin, ILoginResponse } from '../interfaces';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  validateForm: FormGroup;
  mensajes: {[name: string]: {titulo: string; contenido: string};} = {
    success: {titulo:'Login exitoso',  contenido: 'Bienvenido, has ingresado al sistema correctamente.'},
    error: {titulo: 'Error inesperado', contenido: 'Estamos experimentando problemas en la aplicación, por favor intenta de nuevo mas tarde.'},
    fail: {titulo: 'Login ha fallado', contenido: 'Por favor intente nuevamente, asegurese de proporcionar las credenciales correctamente.'},
  }
  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private apiService: APIService,
    private storageManagerService: StorageManagerService,
    private tokenManagerService: TokenManagerService,
    private router: Router,
    private notificationService: NzNotificationService,
  ) {
    //moking the form data to avoid type it
    this.validateForm = this.fb.group({
      login: ['daniel.avila@advantage.com.co', [Validators.email, Validators.required]],
      password: ['Brunofernando123*', [Validators.required]],
    });
  }
  ngOnInit(): void {}

  submitForm(): void {
    console.log('submit', this.validateForm.value);
    this.apiService.login(this.validateForm.value).subscribe(
      (data) => {
        console.log(data);
        const nombre = data?.Account?.DataBeanProperties.Name1 ? data?.Account?.DataBeanProperties.Name1 : '';
        const apellido = data?.Account?.DataBeanProperties.Surname1 ? data?.Account?.DataBeanProperties.Surname1 : '';
        const successResponse: ILoginResponse<IDataLogin> = {
          mensaje: 'Usuario válido',
          success: data?.State === 2? true: false,
          token: 'pendientes por definir',
          data: {
            role: '',
            usuario: nombre + ' ' + apellido,
          },
        };
        this.registrarDataLogin(successResponse);
      },
      (error) => {
        this.handleError('error')
      }
    );
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsPristine();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
  }

  private registrarDataLogin(data: ILoginResponse<IDataLogin>) {
    if (data.success) {
      this.loginService.setLogged(true);
      this.loginService.setDataLogin({
        usuario: data.data.usuario,
        role: data.data.role,
      });
      this.tokenManagerService.set(data.token);
      this.storageManagerService.set('dataLogin', data.data);
      this.notificationService.success(
        this.mensajes['success'].titulo,
        this.mensajes['success'].contenido
      );
      this.router.navigate(['/']);
    } else {
      // notificar el error (no login)
      this.notificationService.error(
        this.mensajes['fail'].titulo,
        this.mensajes['fail'].contenido
      );
    }
  }

  private handleError(errorData: any) {
    // notificar el error
    this.notificationService.error(
      this.mensajes['error'].titulo,
      this.mensajes['error'].contenido
    );
    console.error(errorData);
  }
}

