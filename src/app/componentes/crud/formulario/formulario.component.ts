import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { APIService, IDataProperties } from 'src/app/services/api.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

  validateForm: FormGroup = new FormGroup({});
  data: IDataProperties = {
    Descripcion: '',
    Estado: 0,
    IDPerfilBanco: '',
    IDPropiedades: null,
    Nombre: '',
    Since: '',
    Valor: '',
  };
  constructor(
    private fb: FormBuilder,
    private apiService: APIService,
    private modal: NzModalRef
  ) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      IDPropiedades: [this.data.IDPropiedades, [Validators.required]],
      Nombre: [this.data.Nombre, [Validators.required]],
      Descripcion: [this.data.Descripcion, [Validators.required]],
      Since: [this.data.Since, [Validators.required]],
      IDPerfilBanco: [this.data.IDPerfilBanco, [Validators.required]],
      Valor: [this.data.Valor, [Validators.required]],
      Estado: [this.data.Estado, [Validators.required]],
    });
  }

  submitForm() {
    this.apiService
      .updatePropiedades(this.validateForm.value)
      .subscribe((response) => {
        console.log(response);
        this.apiService.setReload();
        this.modal.destroy();
      });
  }

  destroyModal() {
    console.log('bye');
    this.modal.destroy();
  }

}
