import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Subscription } from 'rxjs';
import { APIService, IDataProperties, IObjectValue } from 'src/app/services/api.service';
import { IDataBeanProperties } from '../../services/api.service';
import { DetallePropertyComponent } from '../detalle-property/detalle-property.component';
import { FormularioComponent } from './formulario/formulario.component';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss'],
})
export class CrudComponent implements OnInit {
  title = 'Management of Properties';

  listSubscription: Subscription = new Subscription();

  textToSearch: string = '';

  entities: IDataProperties[] = [];

  entitiesToShow: IDataProperties[] = [];

  constructor(
    private apiService: APIService,
    private notificationService: NzNotificationService,
    private modalService: NzModalService
  ) { }

  ngOnInit(): void {
    this.getListProperties();
    this.apiService.reload.subscribe(
      () => {
        console.log('recarga');
        this.getListProperties();
      }
    )
  }
  getListProperties() {
    this.listSubscription = this.apiService.getPropiedadesCatalogPorPropiedad().subscribe(
      (response) => {
        this.entities = response;
        this.entitiesToShow = this.entities ? [... this.entities] : [];
        this.listSubscription.unsubscribe();
      },
      (error) => {
        console.error(error);
        this.notificationService.error('Unexpected Error', 'We are facing an unexpected situation, please try later.');
      }
    );
  }
  getProperty(idProperty: IDataProperties): void {
    this.apiService.getPropiedadNumber(idProperty.IDPropiedades ? idProperty.IDPropiedades : 0).subscribe(
      (response) => {
        this.modalService.create({
          nzTitle: 'Detalle Propiedad',
          nzContent: DetallePropertyComponent,
          nzComponentParams: {
            data: response
          },
          nzFooter: null
        });
      },
      (error) => {
        console.error(error);
        this.notificationService.error('Unexpected Error', 'We are facing an unexpected situation, please try later.');
      }
    );
  }

  onTextSearch(text: string) {
    this.textToSearch = text;
    this.entitiesToShow = this.entities?.filter(entity => entity.Nombre.toLowerCase().includes(text.toLowerCase().trim()));
    console.log(text);
  }

  onNewProperty(): void {
    const newProperty: IDataProperties = {
      Descripcion: '',
      Estado: 1,
      IDPerfilBanco: '',
      IDPropiedades: null,
      Nombre: '',
      Since: '',
      Valor: ''
    };
    this.modalService.create({
      nzTitle: 'Formulario Properties',
      nzContent: FormularioComponent,
      nzComponentParams: {
        data: newProperty
      },
    });
  }



  onView(idPropiedad: IDataProperties) {
    console.log('idPropiedad', idPropiedad);
    this.getProperty(idPropiedad);

  }
  onEdit(event: IDataProperties) {
    console.log('onEdit', event);
    this.modalService.create({
      nzTitle: 'Formulario Properties',
      nzContent: FormularioComponent,
      nzComponentParams: {
        data: event
      },
    });
  }

  onDelete(event: IDataProperties) {
    this.modalService.confirm({
      nzTitle: 'Desea eliminar la siguiente propiedad?',
      nzContent: `<b style="color: red;">${event.Nombre}</b>`,
      nzOkText: 'Yes',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOnOk: () => this.deletePropertyeve(event),
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel')
    });
  }

  deletePropertyeve(event: IDataProperties) {
    this.apiService.deletePropiedad(event.IDPropiedades).subscribe(
      (response) => {
        this.apiService.setReload();
        console.log(response)
      }
    )
  }
}


