import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePropertyComponent } from './detalle-property.component';

describe('DetallePropertyComponent', () => {
  let component: DetallePropertyComponent;
  let fixture: ComponentFixture<DetallePropertyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallePropertyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
