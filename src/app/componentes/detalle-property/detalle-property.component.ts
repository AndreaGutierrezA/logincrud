import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { IDataProperties } from '../../services/api.service';

@Component({
  selector: 'app-detalle-property',
  templateUrl: './detalle-property.component.html',
  styleUrls: ['./detalle-property.component.scss']
})
export class DetallePropertyComponent implements OnInit {

  data: IDataProperties = {
    Nombre: '',
    IDPropiedades: 0,
    Since: '',
    Descripcion: '',
    Valor: '',
    IDPerfilBanco: '',
    Estado: 0
  };
  constructor(public modalService: NzModalService) { }

  ngOnInit(): void {
  }

}
