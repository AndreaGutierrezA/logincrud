import { Component, EventEmitter, Input, OnInit, Output,  } from '@angular/core';
import { IDataProperties } from '../../services/api.service';

@Component({
  selector: 'app-table-entities',
  templateUrl: './table-entities.component.html',
  styleUrls: ['./table-entities.component.scss']
})
export class TableEntitiesComponent implements OnInit {

  @Input() entities: IDataProperties[] = [];
  @Output() view: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();
  @Output() delete: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onView(event: any) {
    this.view.emit(event);
  }
  onEdit(event: any) {
    this.edit.emit(event);
  }

  onDelete(event: any) {
    this.delete.emit(event);
  }

}
