import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableEntitiesComponent } from './table-entities.component';

describe('TableEntitiesComponent', () => {
  let component: TableEntitiesComponent;
  let fixture: ComponentFixture<TableEntitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableEntitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableEntitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
