import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header-pagina',
  templateUrl: './header-pagina.component.html',
  styleUrls: ['./header-pagina.component.scss'],
})
export class HeaderPaginaComponent implements OnInit {
  @Input() title = '';

  @Output() newProperty: EventEmitter<void> = new EventEmitter();

  @Output() textToSearch: EventEmitter<string> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  toSearch(text: string) {
    this.textToSearch.emit(text);
  }
}
