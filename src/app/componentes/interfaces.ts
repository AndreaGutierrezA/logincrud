export interface IDataLogin {
  usuario: string;
  role: string;
}

export interface ILoginForm {
  usuario: string;
  clave: string;
}

export interface ILoginResponse<U> extends IResponse<U> {
  token: string;
}

export interface IResponse<U> {
  mensaje: string;
  success: boolean;
  data: U;
}

export interface BackendServiceInterface {
  url: string;
  endpoint: string;
}

export interface IParque {
  id: string;
  nombre: string;
  imagen: string;
  tipo: string;
  ultimaVersion: {
    id: string;
    nombre: string;
  };
}
