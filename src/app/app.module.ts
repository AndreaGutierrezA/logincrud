import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { es_ES } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzTableModule } from 'ng-zorro-antd/table';

import { DemoComponent } from './componentes/demo/demo.component';
import { HeaderComponent } from './componentes/header/header.component';
import { LoginBoxComponent } from './componentes/login-box/login-box.component';
import { LoginComponent } from './componentes/login/login.component';
import { LogoutComponent } from './componentes/logout/logout.component';
import { PrincipalComponent } from './componentes/principal/principal.component';
import { HeaderPaginaComponent } from './componentes/header-pagina/header-pagina.component';
import { CrudComponent } from './componentes/crud/crud.component';
import { TableEntitiesComponent } from './componentes/table-entities/table-entities.component';
import { AuthInterceptor } from './services/AuthInterceptor';
import { DetallePropertyComponent } from './componentes/detalle-property/detalle-property.component';
import { FormularioComponent } from './componentes/crud/formulario/formulario.component';

registerLocaleData(es);

@NgModule({
  declarations: [
    AppComponent,
    DemoComponent,
    HeaderComponent,
    LoginBoxComponent,
    LoginComponent,
    LogoutComponent,
    PrincipalComponent,
    HeaderPaginaComponent,
    CrudComponent,
    TableEntitiesComponent,
    DetallePropertyComponent,
    FormularioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzIconModule,
    NzButtonModule,
    NzTypographyModule,
    NzDividerModule,
    NzGridModule,
    NzDatePickerModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    NzInputNumberModule,
    NzRadioModule,
    NzSwitchModule,
    NzUploadModule,
    NzBadgeModule,
    NzCardModule,
    NzEmptyModule,
    NzImageModule,
    NzListModule,
    NzPopoverModule,
    NzTagModule,
    NzToolTipModule,
    NzAlertModule,
    NzModalModule,
    NzProgressModule,
    NzSpinModule,
    NzResultModule,
    NzNotificationModule,
    NzTableModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: es_ES }, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
