export const environment = {
  production: true,
  backendUrl: 'http://www.remote-server.com',
  tokenField: 'token',
};
